package com.innocare.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utilities {
	
	public WebDriver driver;
	public WebDriverWait wait;
	public int wait_time = 10;
	
	//Initialize the data properties reader 
	public Properties data;
	//Initialize the elements properties reader
	public Properties elements;
	
	//Initialize the browser and data readers
	public void utilities(String browser, String host){
		
		//Initialize the data properties reader 
		data = ReadData.readdatafile();
		//Initialize the elements properties reader
		elements = ReadData.readelementsfile();
		
		//Initialize Chrome browser driver
		if(browser.contentEquals("Chrome")){
			
			ChromeOptions options = new ChromeOptions();
			
			DesiredCapabilities capability = DesiredCapabilities.chrome();
			
			capability.setCapability(ChromeOptions.CAPABILITY, options);
			
			capability.setPlatform(Platform.WINDOWS);
			
			capability.setBrowserName("chrome");
			
			capability.setJavascriptEnabled(true);
			
			try {

				driver = new RemoteWebDriver(new URL(host), capability);
				
			} catch (MalformedURLException e) {

				e.printStackTrace();
			}
		//Initialize Firefox browser driver	
		}else if(browser.contentEquals("Firefox")){
			
			DesiredCapabilities capability = DesiredCapabilities.firefox();
						
			capability.setPlatform(Platform.WINDOWS);
			
			capability.setBrowserName("firefox");
			
			capability.setJavascriptEnabled(true);
			
			try {
				driver = new RemoteWebDriver(new URL(host), capability);
				
			} catch (MalformedURLException e) {
				
				e.printStackTrace();
			}
		//Initialize InternetExplorer browser driver	
		}else if(browser.contentEquals("IE")){
			
			DesiredCapabilities capability = DesiredCapabilities.internetExplorer();
			
			capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			
			capability.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
			
			capability.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
			
			capability.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,true);
			
			capability.setJavascriptEnabled(true);
			
			capability.setPlatform(Platform.WINDOWS);
			
			capability.setBrowserName("internet explorer");
			
			try {
				
				driver = new RemoteWebDriver(new URL(host), capability);
				
			} catch (MalformedURLException e) {
				
				e.printStackTrace();
			}
		}
		wait = new WebDriverWait(driver, wait_time);
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	//Load the Webpage 
	public void load_page(String url) {
		
		//Fetch the Webpage
		driver.get(url);
				
		// Maximize the browser window
		driver.manage().window().maximize();
	}
	
	public void teardown() {

		driver.close();

		driver.quit();

	}
	
	//This is the select element by (xpath, id,
	//cssselector, name, lintext, classname ) section.
	public void select_by_xpath(String xpath) {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath))).click();
	}
	
	public void select_by_id(String id) {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id(id))).click();
	}
	
	public void select_by_cssselector(String cssselector) {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(cssselector))).click();		
	}
	
	public void select_by_name(String name) {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.name(name))).click();
	}

	public void select_by_linktext(String linktext) {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.linkText(linktext))).click();
	}
	
	public void select_by_classname(String classname) {
		
		wait.until(ExpectedConditions.elementToBeClickable(By.className(classname))).click();
	}
	
	//This is the enter text by (xpath, id,
	//cssselector, name, lintext, classname ) section.
	public void type_by_xpath(String xpath, String text){
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath))).click();
		
		driver.findElement(By.xpath(xpath)).clear();
		
		driver.findElement(By.xpath(xpath)).sendKeys(text);
	}
	
	public void type_by_id(String id, String text){
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id(id))).click();
		
		driver.findElement(By.id(id)).clear();
		
		driver.findElement(By.id(id)).sendKeys(text);
	}

	public void type_by_cssselector(String cssselector, String text){
		
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(cssselector))).click();
		
		driver.findElement(By.cssSelector(cssselector)).clear();
		
		driver.findElement(By.cssSelector(cssselector)).sendKeys(text);
	}
	
	public void type_by_name(String name, String text){
		
		wait.until(ExpectedConditions.elementToBeClickable(By.name(name))).click();
		
		driver.findElement(By.name(name)).clear();
		
		driver.findElement(By.name(name)).sendKeys(text);
	}
	
	public void type_by_classname(String classname, String text){
		
		wait.until(ExpectedConditions.elementToBeClickable(By.className(classname))).click();
		
		driver.findElement(By.className(classname)).clear();
		
		driver.findElement(By.className(classname)).sendKeys(text);
	}
	
	public String gettext_by_xpath(String xpath) {

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		return driver.findElement(By.xpath(xpath)).getText();
	}
	
	public String gettext_by_id(String id) {

		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));

		return driver.findElement(By.id(id)).getText();
	}
	
	public String gettext_by_cssselector(String cssselector) {

		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(cssselector)));

		return driver.findElement(By.cssSelector(cssselector)).getText();
	}
	
	public String gettext_by_name(String name) {

		wait.until(ExpectedConditions.presenceOfElementLocated(By.name(name)));

		return driver.findElement(By.name(name)).getText();
	}
	
	public boolean  validate_element_present_by_xpath(String xpath) {
		
		boolean displayed;
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		
		return displayed = driver.findElement(By.xpath(xpath)).isDisplayed();
	}
	public boolean  validate_element_present_by_id(String id) {
		
		boolean displayed;
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
		
		return displayed = driver.findElement(By.id(id)).isDisplayed();
	}
	
	public boolean  validate_element_present_by_cssselector(String cssselector) {
		
		boolean displayed;
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(cssselector)));
		
		return displayed = driver.findElement(By.cssSelector(cssselector)).isDisplayed();
	}
	
	public boolean  validate_element_present_by_name(String name) {
		
		boolean displayed;
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name(name)));
		
		return displayed = driver.findElement(By.name(name)).isDisplayed();
	}
	
	public void hover_by_xpath(String xpath) {

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		Actions action = new Actions(driver);

		action.moveToElement(driver.findElement(By.xpath(xpath))).build().perform();
	}

	public void hover_by_id(String id) {

		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));

		Actions action = new Actions(driver);

		action.moveToElement(driver.findElement(By.id(id))).build().perform();
	}

	public void hover_by_cssselector(String cssselector) {

		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(cssselector)));

		Actions action = new Actions(driver);

		action.moveToElement(driver.findElement(By.cssSelector(cssselector))).build().perform();
	}

	public void hover_by_name(String name) {

		wait.until(ExpectedConditions.presenceOfElementLocated(By.name(name)));

		Actions action = new Actions(driver);

		action.moveToElement(driver.findElement(By.name(name))).build().perform();
	}
	
	public void hover_by_classname(String classname) {

		wait.until(ExpectedConditions.presenceOfElementLocated(By.className(classname)));

		Actions action = new Actions(driver);

		action.moveToElement(driver.findElement(By.className(classname))).build().perform();
	}
	
	public void MouseOver_by_xpath(String xpath) {

		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		
		try
		{
			String mouseOver = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');"
					+ "evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);}"
					+ " else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
			
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			
			jse.executeScript(mouseOver, driver.findElement(By.xpath(xpath)));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void MouseOver_by_id(String id) {

		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
		
		try
		{
			String mouseOver = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');"
					+ "evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);}"
					+ " else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
			
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			
			jse.executeScript(mouseOver, driver.findElement(By.id(id)));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void MouseOver_by_name(String name) {

		wait.until(ExpectedConditions.presenceOfElementLocated(By.name(name)));
		
		try
		{
			String mouseOver = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');"
					+ "evObj.initEvent('mouseover', true, false); arguments[0].dispatchEvent(evObj);}"
					+ " else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
			
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			
			jse.executeScript(mouseOver, driver.findElement(By.name(name)));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public String geturl(){
		
		return driver.getCurrentUrl();
	}
	
	public String getpartialurl(int start, int end) {
		
		return driver.getCurrentUrl().substring(start, end);
	}
	
	public void scrollTo_by_xpath(String xpath) {
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath(xpath)));       
    }
	
	public void scrollTo_by_id(String id) {
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.id(id)));       
    }
	
	public void scrollTo_by_name(String name) {
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.name(name)));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.name(name)));       
    }

	public void scroll(int scroll) {
		
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		
		jse.executeScript("scroll(0, scroll)");       
    }
	
	public void delete_coockies(){
		
		driver.manage().deleteAllCookies();
	}
	
	public void refresh_page() {
		
		driver.navigate().refresh();
	}
	
	public void navigate_back() {

		driver.navigate().back();
	}
	
	public void sleep(long time) {
		
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		e.printStackTrace(); 
		}
	}

}
