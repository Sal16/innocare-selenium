package com.innocare.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ReadData {
	
	public static String localUIPath = System.getProperty("user.dir") + "/UIElements/";
	public static String localTestDataPath = System.getProperty("user.dir") + "/TestData/";
	
	public static String TestDataFile 	= localTestDataPath + "TestData.properties";
	public static String UIElementsFile = localUIPath + "UIElements.properties";
	
	public static Properties readdatafile (){
		File file = new File(TestDataFile);
		  
		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();
		//load properties file
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}
	
	public static Properties readelementsfile (){
		Properties prop = new Properties();
		
		File file = new File(UIElementsFile);
		  
		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//load properties file
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}
	
	public static Properties readdatafile (String[] list_of_files){
		//Consolidated Properties
		Properties prop = new Properties();
		//Temporary Properties Placeholder
		Properties tempProp;
		
		File file;
		for (String filename : list_of_files) {
			tempProp = new Properties();
			file = new File(localTestDataPath + filename);
			  
			FileInputStream fileInput = null;
			try {
				fileInput = new FileInputStream(file);

				//load properties file
				try {
					tempProp.load(fileInput);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} finally {
				try {
					//Close FileInputStream
					fileInput.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			//Add all entry to main properties
			prop.putAll(tempProp);
		}
		
		//Add Default to main properties
		prop.putAll(readdatafile());
		
		return prop;
	}
	
	public static Properties readelementsfile (String[] additionalUI){
		//Consolidated Properties
		Properties prop = new Properties();
		//Temporary Properties Placeholder
		Properties tempProp;
		
		File file;
		for (String filename : additionalUI) {
			tempProp = new Properties();
			file = new File(localUIPath + filename);
			  
			FileInputStream fileInput = null;
			try {
				fileInput = new FileInputStream(file);

				//load properties file
				try {
					tempProp.load(fileInput);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				System.out.println (filename + " not found. Data not loaded.");
			} finally {
				try {
					//Close FileInputStream
					fileInput.close();
				} catch (IOException e) {
					//e.printStackTrace();
				}
			}
			//Add all entry to main properties
			prop.putAll(tempProp);
		}
		
		//Add Default to main properties
		prop.putAll(readelementsfile());
		
		return prop;
	}

}
