package com.innocare.org_management;

import java.util.Properties;
import java.util.UUID;

//import org.junit.AfterClass;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;

import com.innocare.utils.ReadData;
import com.innocare.utils.Utilities;

public class Create_org extends Utilities{
	
	//Initialize the data properties reader 
	public Properties data = ReadData.readdatafile();
	//Initialize the elements properties reader
	public Properties elements = ReadData.readelementsfile();
	
	String adminname;
	String adminemail;
	String firstclinicname;
	String secondclinicname;
	String firstclinicemail;
	String secondclinicemail;
	String clinicaddress;
	String clinicphone;
	String clinicfax;
	String dateofbirth;
	
	@Parameters({ "Browser", "URL" })
	@BeforeClass
	public void beforeClass(String Browser, String URL) {
		
		utilities(Browser, data.getProperty("hostserver"));

		load_page(URL);
	}
	
	@Test
	public void create_org() {
		
		adminname = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 30);
		
		/*adminname = Long.toString(uuid.getMostSignificantBits(), 30) +
				Long.toString(uuid.getLeastSignificantBits(), 30);*/
		
		adminemail = adminname + "@slalom.org";
		
		//adminname = UUID.randomUUID().toString();
		
		//adminemail = UUID.randomUUID().toString() + "@slalom.org";
		
		//Create account
		type_by_xpath(elements.getProperty("createadminname"), adminname);
		
		type_by_xpath(elements.getProperty("createadminemail"), adminemail);

		type_by_xpath(elements.getProperty("createadminpassword"), "password");
		
		type_by_xpath(elements.getProperty("confirmadmipassword"), "password");
		
		select_by_xpath(elements.getProperty("createnewuserbtn"));
		
		Assert.assertTrue(validate_element_present_by_xpath(elements.getProperty("createnewuserbtn")), "Fail to create user");
		
		String orgname = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 30);
		
		String firstclinic = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10);
		
		String secondclinic = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10);
		
		clinicphone = "234-567-9874";
		
		clinicfax = "256-877-3974";
		
		clinicaddress = "8 King St East, Toronto, Ontario, Canada";
		
		/*String orgname = Long.toString(orgname.getMostSignificantBits(), 40) +
				Long.toString(orgname.getLeastSignificantBits(), 40);*/
		
		/*firstclinicname = Long.toString(15) +
				Long.toString(firstclinic.getLeastSignificantBits(), 15);
		
		secondclinicname = Long.toString(secondclinic.getMostSignificantBits(), 15) +
				Long.toString(secondclinic.getLeastSignificantBits(), 15);*/
		firstclinicname = firstclinic;
		
		secondclinicname = secondclinic;
		
		firstclinicemail = firstclinic + "@slalom.org";
		
		secondclinicemail = secondclinic + "@slalom.org";
		
		select_by_xpath(elements.getProperty("startsubscreptionbtn"));
		
		Assert.assertEquals(gettext_by_xpath(elements.getProperty("step2setuptag")), data.getProperty("step2tag"));
		
		//Organization name
		type_by_xpath(elements.getProperty("createorgname"), orgname);
		
		//Add first clinic
		type_by_xpath(elements.getProperty("firstclinicname"), firstclinicname);
		
		type_by_xpath(elements.getProperty("firstclinicemail"), firstclinicemail);
		
		type_by_xpath(elements.getProperty("firstclinicphone"), clinicphone);
		
		type_by_xpath(elements.getProperty("firstclinicfax"), clinicfax);
		
		type_by_xpath(elements.getProperty("firstclinicaddress"), clinicaddress);
		
		//Add second clinic
		select_by_xpath(elements.getProperty("addsecondclinicbtn"));
		
		type_by_xpath(elements.getProperty("secondclinicname"), secondclinicname);
		
		type_by_xpath(elements.getProperty("secondclinicemail"), secondclinicemail);
		
		type_by_xpath(elements.getProperty("seconfclinicphone"), clinicphone);
		
		type_by_xpath(elements.getProperty("secondclinicfax"), clinicfax);
		
		type_by_xpath(elements.getProperty("secondclinicaddress"), clinicaddress);
		
		select_by_xpath(elements.getProperty("continubtn"));
		
		Assert.assertEquals(gettext_by_xpath(elements.getProperty("step3setuptag")), data.getProperty("step3tag"));
		
		//Add first user
		type_by_xpath(elements.getProperty("firstusername"), firstclinicname);
		
		type_by_xpath(elements.getProperty("firstuseremail"), firstclinicemail);
		
		select_by_xpath(elements.getProperty("firstusertypemenu"));
		
		select_by_xpath(elements.getProperty("firstclinicianusertype"));
		
		select_by_xpath(elements.getProperty("firstpractitionertypemenu"));
		
		select_by_xpath(elements.getProperty("firstpractitionertype"));
		
		select_by_xpath(elements.getProperty("firstclinicmenu"));
		
		select_by_xpath(elements.getProperty("firstclinicmenuoption"));
		
		select_by_xpath(elements.getProperty("addseconduserbtn"));
		
		//Add second user
		type_by_xpath(elements.getProperty("secondusername"), secondclinicname);
		
		type_by_xpath(elements.getProperty("seconduseremail"), secondclinicemail);
		
		select_by_xpath(elements.getProperty("secondusertypemenu"));
		
		select_by_xpath(elements.getProperty("secondclinicianusertype"));
		
		select_by_xpath(elements.getProperty("secondpractitionertypemenu"));
		
		select_by_xpath(elements.getProperty("secondpractitionertype"));
		
		select_by_xpath(elements.getProperty("secondclinicmenu"));
		
		select_by_xpath(elements.getProperty("secondclinicmenuoption"));
		
		select_by_xpath(elements.getProperty("continubtn"));
		
		Assert.assertEquals(gettext_by_xpath(elements.getProperty("step4setuptag")), data.getProperty("step4tag"));
		
		select_by_xpath(elements.getProperty("continubtn"));
		
		Assert.assertTrue(validate_element_present_by_xpath(elements.getProperty("nocheckedpatient")), "Fail to Subscreption");
	}
	
	@Test (dependsOnMethods = { "create_org" })
	public void add_patient() {
		
		dateofbirth = "16/03/1987";
		
		select_by_xpath(elements.getProperty("searchpatient"));
		
		select_by_xpath(elements.getProperty("Addpatientbtn"));
		
		type_by_xpath(elements.getProperty("patientfirstname"), firstclinicname);
		
		type_by_xpath(elements.getProperty("patientlastname"), secondclinicname);
		
		type_by_xpath(elements.getProperty("patientbirthdate"), dateofbirth);
		
		select_by_xpath(elements.getProperty("patientgendermenu"));
		
		select_by_xpath(elements.getProperty("patientgenderoption"));
		
		type_by_xpath(elements.getProperty("patientaddress"), clinicaddress);
		
		select_by_xpath(elements.getProperty("patientcountrymenu"));

		select_by_xpath(elements.getProperty("patientcountryoption"));
		
		type_by_xpath(elements.getProperty("patientcity"), "Toronto");
		
		select_by_xpath(elements.getProperty("patientprovincemenu"));
		
		select_by_xpath(elements.getProperty("patientprovinceoption"));
		
		type_by_xpath(elements.getProperty("patientpostalcode"), "M2K 3H8");
		
		type_by_xpath(elements.getProperty("patientdayphone"), clinicphone);
		
		type_by_xpath(elements.getProperty("patientmobile"), clinicphone);
		
		type_by_xpath(elements.getProperty("patientemail"), secondclinicemail);
		
		type_by_xpath(elements.getProperty("patientphysicianname"), firstclinicname);
		
		type_by_xpath(elements.getProperty("patientphysiciannumber"), clinicfax);
		
		type_by_xpath(elements.getProperty("patientphysicianfax"), clinicfax);
		
		type_by_xpath(elements.getProperty("patientaccountnotes"), data.getProperty("patientnotes"));
		
		select_by_xpath(elements.getProperty("savepatient"));
		
		type_by_xpath(elements.getProperty("search"), firstclinicname);
		
		select_by_xpath(elements.getProperty("searchpatient"));
		
		Assert.assertEquals(gettext_by_xpath(elements.getProperty("newpatientname")), firstclinicname + " " + secondclinicname, "Patient not found in the system");
	}
	
	@Test (dependsOnMethods = { "add_patient" })
	public void create_patient_checkin() {
		
		type_by_xpath(elements.getProperty("search"), firstclinicname);
		
		select_by_xpath(elements.getProperty("checkinbtn"));
		
		select_by_xpath(elements.getProperty("Appointmenttimemenu"));
		
		select_by_xpath(elements.getProperty("appointmenttimeoption"));
		
		select_by_xpath(elements.getProperty("appointmentdurationmenu"));
		
		select_by_xpath(elements.getProperty("appointmentdurationoption"));
		
		select_by_xpath(elements.getProperty("providertypemenu"));
		
		select_by_xpath(elements.getProperty("providertypeoption"));
		
		select_by_xpath(elements.getProperty("clinicmenu"));
		
		select_by_xpath(elements.getProperty("clinicmenuoption"));
		
		select_by_xpath(elements.getProperty("fundingstreammenu"));
		
		select_by_xpath(elements.getProperty("fundingstreamoption"));
		
		type_by_xpath(elements.getProperty("createcheckinnotes"), data.getProperty("patientnotes"));
		
		select_by_xpath(elements.getProperty("continubtn"));
		
		int index = gettext_by_xpath(elements.getProperty("checkedpatient")).indexOf("(");
		
		Assert.assertEquals(gettext_by_xpath(elements.getProperty("checkedpatient")).substring(0, index), firstclinicname + " " + secondclinicname, "Appointment not found");
		
		Assert.assertEquals(gettext_by_xpath(elements.getProperty("beginappointmentbtn")), "Begin Appointment", "Fail to create appointment");
		
		sleep(9000);
	}
	
	@Test (dependsOnMethods = { "create_patient_checkin" })
	public void start_new_assessment() {
		
		select_by_xpath(elements.getProperty("beginappointmentbtn"));
		
		select_by_xpath(elements.getProperty("newassessmentbtn"));
		
		type_by_xpath(elements.getProperty("assessmentdate"), "20/06/2015");

		type_by_xpath(elements.getProperty("assessmentprovidername"), firstclinicname);
		
		select_by_xpath(elements.getProperty("primaryconcernmenu"));
		
		select_by_xpath(elements.getProperty("primaryconceroption"));
		
		type_by_xpath(elements.getProperty("contraindication"), "Allergic reactions to penicillin");
		
		type_by_xpath(elements.getProperty("currenthistory"), "High blood pressure");
		
		type_by_xpath(elements.getProperty("previoushistory"), "Heart attack");
		
		type_by_xpath(elements.getProperty("generalhealthnote"), data.getProperty("patientnotes"));
		
		select_by_xpath(elements.getProperty("saveassessment"));
		
		Assert.assertEquals(gettext_by_xpath(elements.getProperty("appointmenttime")), "10:00 - 10:45 am", "Appointment time not found");
		
		Assert.assertEquals(gettext_by_xpath(elements.getProperty("assessmenttype")), "Assessment Note", "Assessment note not found");
		
		Assert.assertEquals(gettext_by_xpath(elements.getProperty("veiwnotebtn")), "View Note", "View note button not found");
		
	}
	
	@AfterClass
	public void AfterClass() {
		
		teardown();
		  
	}
}